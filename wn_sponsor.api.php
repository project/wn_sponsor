<?php

/**
 * @file
 * Hooks specific to the Will Nice Sponsor module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 *
 * The hook for successful sponsorship payment facilitates other modules to
 * execute actions triggered after successful sponsorship,such as opening read
 * permissions for a certain article.
 * 赞助付款成功的钩子，便于其他模块执行赞助成功后触发的动作，比如开通某文章的读取权限。
 *
 * @code
 * $data = [
 *   'content_id' => Sponsor collection content ID,
 *   'comment_id' => Comment ID(Sponsor Order),
 *   'user' => [
 *      'name' => Sponsor order owner real name,
 *      'wechat' => Sponsor order owner WeChat number,
 *      'phone' => Sponsor order owner contact phone,
 *   ],
 *   'amount' => The sponsor amount user paid,
 *   'time' => The sponsor submitted time,
 * ];
 * \Drupal::database()->insert('node_access')->fields($record)->execute();
 *
 * @param array $data
 *
 * @return void
 *
 */
function hook_wn_sponsor_payment_success(array &$data) {
  $cid = $data['comment_id'];
  if ($sponsor_order = \Drupal\comment\Entity\Comment::load($cid)) {
    $status = $sponsor_order->get('field_wn_sponsorship_paid')->value;
    if(in_array($status, [YK_ORDER_STATE_SUCCESS, YK_ORDER_STATE_REFUND_PROGRESS, YK_ORDER_STATE_REFUND_PART])){
      $user = \Drupal::currentUser();
      // Opening this user's read permissions for a certain article.
      // ...
      // 为该用户开通读取某篇文章的权限。
      // ...
    }
  }
}

/**
 * Hook for successful sponsorship refund, used for processing after refund.
 * 赞助退款成功的钩子，用于退款后的处理。
 *
 * @code
 * $data = [
 *   'content_id' => Sponsor collection content ID,
 *   'comment_id' => Comment ID(Sponsor Order),
 *   'user' => [
 *      'name' => Sponsor order owner real name,
 *      'wechat' => Sponsor order owner WeChat number,
 *      'phone' => Sponsor order owner contact phone,
 *   ],
 *   'amount' => The sponsor amount user paid,
 *   'time' => The sponsor submitted time,
 * ];
 * \Drupal::database()->insert('node_access')->fields($record)->execute();
 *
 * @param array $data
 */
function hook_wn_sponsor_refund_success(array &$data) {
  // Processing after refund.
  // ...
  // 退款后的处理。
  // ...
}

/**
 * @} End of "addtogroup hooks".
 */
