<?php
/**
 * 开发公司：未来很美（深圳）科技有限公司 (www.will-nice.com)
 * 开发者：云客 (www.indrupal.com)
 * 微信号（WeChat）：indrupal
 * Email:phpworld@qq.com
 *
 */

namespace Drupal\wn_sponsor;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\Url;

/**
 * Ensures that renew modules cannot be uninstalled.
 */
class WnSponsorUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ContentUninstallValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    if ($module == "wn_sponsor") {
      $wn_sponsor_comment_count = \Drupal::entityQuery('comment')
        ->accessCheck(TRUE)
        ->condition('comment_type', 'wn_sponsor_comment')
        ->count()
        ->execute();
      $wn_refund_information_count = \Drupal::entityQuery('node')
        ->accessCheck(TRUE)
        ->condition('type', 'wn_refund_information')
        ->count()
        ->execute();
      $wn_sponsor_count = \Drupal::entityQuery('node')
        ->accessCheck(TRUE)
        ->condition('type', 'wn_sponsor')
        ->count()
        ->execute();
      if ($wn_sponsor_comment_count > 0) {
        $reasons[] = $this->t('There is sponsor order comments. <a href=":url">Remove @entity_type_plural</a>.', [
          '@entity_type' => 'comment',
          '@entity_type_plural' => 'comments',
          ':url' => Url::fromRoute('wn_sponsor.prepare_modules_entity_uninstall', ['entity_type_id' => 'comment', 'bundle_id' => 'wn_sponsor_comment'])
            ->toString(),
        ]);
      }
      if ($wn_refund_information_count > 0) {
        $reasons[] = $this->t('There is sponsorship refund nodes. <a href=":url">Remove @entity_type_plural</a>.', [
          '@entity_type' => 'node',
          '@entity_type_plural' => 'nodes',
          ':url' => Url::fromRoute('wn_sponsor.prepare_modules_entity_uninstall', ['entity_type_id' => 'node', 'bundle_id' => 'wn_refund_information'])
            ->toString(),
        ]);
      }
      if ($wn_sponsor_count > 0) {
        $reasons[] = $this->t('There is sponsor nodes. <a href=":url">Remove @entity_type_plural</a>.', [
          '@entity_type' => 'node',
          '@entity_type_plural' => 'nodes',
          ':url' => Url::fromRoute('wn_sponsor.prepare_modules_entity_uninstall', ['entity_type_id' => 'node', 'bundle_id' => 'wn_sponsor'])
            ->toString(),
        ]);
      }
    }
    return $reasons;
  }

}
