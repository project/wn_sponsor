<?php

namespace Drupal\wn_sponsor\Plugin\views\area;

use Drupal\comment\CommentInterface;
use Drupal\node\Entity\Node;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sponsor statistics area handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("wn_sponsor_statistics_area")
 */
class WnSponsorStatisticsArea extends AreaPluginBase {

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @inheritDoc
   */
  public function render($empty = FALSE) {
    $search_title = \Drupal::request()->query->get('title');
    $query = \Drupal::entityQueryAggregate('comment');
    $query->accessCheck(TRUE)
      ->aggregate('field_wn_order_number', 'count')
      ->aggregate('field_wn_sponsorship_money', 'sum')
      ->aggregate('field_wn_sponsorship_money', 'avg')
      ->aggregate('field_wn_sponsorship_money', 'max')
      ->aggregate('field_wn_sponsorship_money', 'min')
      ->condition('field_wn_sponsorship_paid', [
        YK_ORDER_STATE_SUCCESS,
        YK_ORDER_STATE_REFUND_PROGRESS,
        YK_ORDER_STATE_REFUND_PART,
      ], 'IN')
      ->condition('comment_type', 'wn_sponsor_comment')
      ->condition('status', CommentInterface::PUBLISHED);
    if (!empty($search_title)) {
      $or_condition = $query->orConditionGroup();
      $nids = \Drupal::entityQuery('node')
        ->accessCheck(TRUE)
        ->condition('type', 'wn_sponsor')
        ->condition('title', $search_title, 'CONTAINS')
        ->execute();
      if (!empty($nids)) {
        foreach ($nids as $nid) {
          $or_condition->condition('entity_id', $nid);
        }
        $query->condition($or_condition);
      }
    }
    $total = $query->execute();
    $data = [
      'total' => $total,
    ];
    $build = [
      '#theme' => 'wn_sponsor_statistics',
      '#data' => $data,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    return $build;
  }

}