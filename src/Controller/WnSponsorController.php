<?php

/**
 *  WnSponsorController
 *  by:lijiacheng
 */

namespace Drupal\wn_sponsor\Controller;

use chillerlan\QRCode\QRCode;
use Drupal\comment\CommentInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

class WnSponsorController extends ControllerBase {

  public function showCertificate(NodeInterface $node, CommentInterface $comment = NULL) {
    $field_wn_certificate_content = '';
    $certificate_status = 0;
    $field_wn_certificate_seal = NULL;
    $field_wn_cert_padding_block = 0;
    $field_wn_cert_padding_inline = 0;
    $field_wn_cert_background_img = NULL;
    $field_wn_certificate_seal_right = 0;
    $field_wn_cert_seal_bottom = 0;
    $wn_qrcode_query_image = NULL;
    $wn_qrcode_sponsor_image = NULL;
    $field_wn_qrcode_wh = 0;
    $field_wn_qrcode_display = 0;
    $field_wn_qrcode_left = 0;
    $field_wn_qrcode_bottom = 0;
    $destination = \Drupal::request()->query->get('destination');

    $field_wn_certificate_content = $node->get('field_wn_certificate_content')->value;
    if (!empty($comment)) {
      if ($comment->isPublished()) {
        $certificate_status = 1;
        $token = \Drupal::token();
        $field_wn_certificate_content = $token->replace($field_wn_certificate_content);
        $qrcode_query_url = Url::fromRoute('wn_sponsor.certificate', [
          'node' => $node->id(),
          'comment' => $comment->id(),
        ], [
          'absolute' => TRUE,
        ])->toString();
        $wn_qrcode_query_image = (new QRCode())->render($qrcode_query_url);
      }
      else {
        $certificate_status = -1;
      }
    }
    $field_wn_cert_padding_block = $node->get('field_wn_cert_padding_block')->value;
    $field_wn_cert_padding_inline = $node->get('field_wn_cert_padding_inline')->value;
    $field_wn_certificate_seal_right = $node->get('field_wn_certificate_seal_right')->value;
    $field_wn_cert_seal_bottom = $node->get('field_wn_cert_seal_bottom')->value;
    if (!$node->get('field_wn_certificate_seal')->isEmpty()) {
      $field_wn_certificate_seal = \Drupal::service('file_url_generator')
        ->generate($node->get('field_wn_certificate_seal')->entity->getFileUri())
        ->toString();
    }
    if (!$node->get('field_wn_cert_background_img')->isEmpty()) {
      $field_wn_cert_background_img = \Drupal::service('file_url_generator')
        ->generate($node->get('field_wn_cert_background_img')->entity->getFileUri())
        ->toString();
    }
    $sponsor_content_url = Url::fromRoute('entity.node.canonical', [
      'node' => $node->id(),
    ], [
      'absolute' => TRUE,
    ])->toString();
    $wn_qrcode_sponsor_image = (new QRCode())->render($sponsor_content_url);
    $field_wn_qrcode_wh = $node->get('field_wn_qrcode_wh')->value;
    $field_wn_qrcode_display = $node->get('field_wn_qrcode_display')->value;
    $field_wn_qrcode_space = $node->get('field_wn_qrcode_space')->value;
    $field_wn_qrcode_left = $node->get('field_wn_qrcode_left')->value;
    $field_wn_qrcode_bottom = $node->get('field_wn_qrcode_bottom')->value;
    $build = [
      '#theme' => 'wn_sponsor_certificate',
      '#destination' => $destination,
      '#content' => $field_wn_certificate_content,
      '#certificate_status' => $certificate_status,
      '#certificate_seal' => $field_wn_certificate_seal,
      '#certificate_padding_block' => $field_wn_cert_padding_block,
      '#certificate_padding_inline' => $field_wn_cert_padding_inline,
      '#certificate_background_img' => $field_wn_cert_background_img,
      '#certificate_seal_right' => $field_wn_certificate_seal_right,
      '#certificate_seal_bottom' => $field_wn_cert_seal_bottom,
      '#wn_qrcode_query_image' => $wn_qrcode_query_image,
      '#wn_qrcode_sponsor_image' => $wn_qrcode_sponsor_image,
      '#wn_qrcode_wh' => $field_wn_qrcode_wh,
      '#wn_qrcode_display' => $field_wn_qrcode_display,
      '#wn_qrcode_space' => $field_wn_qrcode_space,
      '#wn_qrcode_left' => $field_wn_qrcode_left,
      '#wn_qrcode_bottom' => $field_wn_qrcode_bottom,
      '#wn_sponsor_content_url' => Url::fromRoute('entity.node.canonical', [
        'node' => $node->id(),
      ], [])->toString(),
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    $build['#attached']['library'][] = "wn_sponsor/show_certificate";
    $build['#attached']['drupalSettings']['wn_sponsor_content_redirect'] = Url::fromRoute('entity.node.canonical', ['node' => $node->id()], [])
      ->toString();
    return $build;
  }

}