<?php
/**
 * @file
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\Entity\Node;

/**
 * Implements hook_token_info().
 */
function wn_sponsor_token_info() {
  $types['comment'] = [
    'name' => t("Will Nice Sponsor Token"),
    'description' => t("Customized placeholders provided by the Future Beautiful Sponsorship Module"),
  ];
  $comment['field_wn_sponsor_name'] = [
    'name' => t("Sponsor Name"),
    'description' => t("Name of sponsor with sponsorship behavior"),
  ];
  $comment['field_wn_sponsor_wechat'] = [
    'name' => t("Sponsor WeChat"),
    'description' => t("WeChat of sponsor with sponsorship behavior"),
  ];
  $comment['field_wn_sponsor_phone'] = [
    'name' => t("Sponsor Phone"),
    'description' => t("Phone number of sponsor with sponsorship behavior"),
  ];
  $comment['field_wn_sponsorship_paid'] = [
    'name' => t("Sponsorship Paid"),
    'description' => t("0 | Waiting for payment, 1 | Successful payment, 2 | Payment failed, 3 | Refund in progress, 4 | Partial refund, 5 | Full refund"),
  ];
  $comment['comment_body'] = [
    'name' => t("Sponsor Comment"),
    'description' => t("Sponsor comment written by sponsors when paid for sponsorship."),
  ];
  $comment['field_wn_order_number'] = [
    'name' => t("Sponsor Order Number"),
    'description' => t("Sponsor order number paid by sponsors."),
  ];
  $comment['field_wn_sponsorship_money'] = [
    'name' => t("Sponsorship Money"),
    'description' => t("Amount of sponsor paid for sponsorship content."),
  ];
  $comment['field_wn_hide_sponsorship_info'] = [
    'name' => t("If Hide Sponsorship Info"),
    'description' => t("If true hide sponsorship information."),
  ];
  return [
    'types' => $types,
    'tokens' => [
      'comment' => $comment,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function wn_sponsor_tokens($type, $tokens, array $data, array $options,
  BubbleableMetadata $bubbleable_metadata
) {
  $replacements = [];
  if ($type == 'comment') {
    $comment = \Drupal::routeMatch()->getParameter('comment');
    if (!empty($comment)) {
      $nid = $comment->getCommentedEntityId();
      $node = Node::load($nid);
      $if_hide_cert_personal_info = FALSE;
      if (!$node->get('field_wn_hide_cert_personal_info')->isEmpty()) {
        $if_hide_cert_personal_info = $node->get('field_wn_hide_cert_personal_info')->value;
      }
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'created':
            $replacements[$original] = date('Y年m月d日', $comment->get('field_wn_sponsorship_time')->value);
            break;
          case 'cert_id':
            $replacements[$original] = date('ymd', $comment->get('field_wn_sponsorship_time')->value) . $comment->id();;
            break;
          case 'field_wn_sponsor_name':
            $replacements[$original] = $comment->get('field_wn_sponsor_name')->value;
            break;
          case 'field_wn_sponsor_wechat':
            $wn_sponsor_wechat = $comment->get('field_wn_sponsor_wechat')->value;
            if ($if_hide_cert_personal_info && !empty($wn_sponsor_wechat)) {
              $display_len = floor(strlen($wn_sponsor_wechat) / 2);
              $asterisk_len = strlen($wn_sponsor_wechat) - $display_len;
              $wn_sponsor_wechat = substr($wn_sponsor_wechat, 0, $display_len) . str_repeat('*', $asterisk_len);
            }
            $replacements[$original] = $wn_sponsor_wechat;
            break;
          case 'field_wn_sponsor_phone':
            $wn_sponsor_phone = $comment->get('field_wn_sponsor_phone')->value;
            if ($if_hide_cert_personal_info && !empty($wn_sponsor_phone)) {
              $wn_sponsor_phone = substr($wn_sponsor_phone, 0, 3) . '****' . substr($wn_sponsor_phone, 7);
            }
            $replacements[$original] = $wn_sponsor_phone;
            break;
          case 'field_wn_sponsorship_paid':
            $replacements[$original] = $comment->get('field_wn_sponsorship_paid')->value;
            break;
          case 'comment_body':
            $replacements[$original] = $comment->get('comment_body')->value;
            break;
          case 'field_wn_order_number':
            $replacements[$original] = $comment->get('field_wn_order_number')->value;
            break;
          case 'field_wn_sponsorship_money':
            $replacements[$original] = $comment->get('field_wn_sponsorship_money')->value;
            break;
          case 'field_wn_hide_sponsorship_info':
            $replacements[$original] = $comment->get('field_wn_hide_sponsorship_info')->value;
            break;
        }
      }
    }
  }
  //Return the result so that we can now use the token.
  return $replacements;
}