(function (Drupal) {
  // On the sponsorship form page, click on the amount option to change the sponsorship amount.
  // 赞助表单页面，点击金额选项后更改赞助金额。
  Drupal.behaviors.sponsor_amount_options = {
    attach: function (context, settings) {
      once('sponsor_amount_options', '#sponsor-amount-items .card').forEach(function (element) {
        element.addEventListener('click', function(){
          let money;
          money = element.querySelector('.number-of-money').textContent;
          document.getElementById('edit-sponsorship-form-field-wn-sponsorship-money-0-value').value = money;
        })
      });
    }
  };
}(Drupal));
