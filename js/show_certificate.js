(function (Drupal, drupalSettings) {
  /*
    The certificate details page JS allows the width and height of the certificate display section
    to be equivalent to the actual uploaded certificate background image, in order to ensure that
    the final downloaded image is consistent with the effect seen on the webpage.
  */
  /*
    证书详情页面JS，让证书显示部分的宽高与实际上传的证书背景图片相等，目的是为了最终下载的图片与网页中所见的效果是一致的。
  */
  window.onload = function () {
    once('show_certificate', '#wn-sponsor-overlay').forEach(function (element) {
      // set div container width and height
      // 设置容器宽高
      const certificate_background = document.getElementById('certificate-background');
      const wn_sponsor_certificate = document.getElementById('wn-sponsor-certificate');
      element.style.width = certificate_background.offsetWidth + 'px !important';
      element.style.height = certificate_background.offsetHeight + 'px !important';
      wn_sponsor_certificate.style.width = certificate_background.offsetWidth + 'px !important';
      wn_sponsor_certificate.style.height = certificate_background.offsetHeight + 'px !important';

      // Download certificate
      // 下载证书
      const download_button = document.getElementById('download-certificate');
      download_button.addEventListener('click', function () {
        htmlToImage.toJpeg(document.getElementById('wn-sponsor-overlay'), { quality: 1 })
          .then(function (dataUrl) {
            let link = document.createElement('a');
            link.download = 'certificate.jpeg';
            link.href = dataUrl;
            link.click();
            window.location.href = drupalSettings.wn_sponsor_content_redirect;
          });
      });

      const container_ele = document.getElementById('wn-sponsor-cert-container');
      const scale_ele = document.getElementById('wn-sponsor-scale');
      scale_ele.style.transform = 'scale(' + container_ele.offsetWidth / scale_ele.offsetWidth + ')';
      scale_ele.style.webkitTransform = 'scale(' + container_ele.offsetWidth / scale_ele.offsetWidth + ')';
      scale_ele.style.transformOrigin = 'top left';
    });
  };
}(Drupal, drupalSettings));
