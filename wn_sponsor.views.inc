<?php

/**
 * Implements hook_views_data().
 */
function wn_sponsor_views_data() {
  $data['views']['wn_sponsor_statistics_area'] = [
    'title' => 'Will Nice Sponsor Statistics',
    'help' => 'Provides a sponsor statistics area.',
    'area' => [
      'id' => 'wn_sponsor_statistics_area',
    ],
  ];
  $data['comment']['wn_sponsor_certificate_number'] = [
    'title' => t('Certificate Number'),
    'filter' => [
      'title' => t('Sponsor Certificate Number - wn_sponsor Filter'),
      'field' => 'field_wn_sponsorship_comment',
      'id' => 'wn_sponsor_certificate_number',
    ],
  ];
  return $data;
}